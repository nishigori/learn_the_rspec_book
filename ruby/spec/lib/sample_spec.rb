# -*- encoding: utf-8 -*-
require 'spec_helper'

describe 'RSpec Greeter' do
  let(:greeter) { RSpecGreeter.new }

  describe '#say' do
    subject { greeter.greet(greeting) }
    let(:greeting) { 'Hello' }
    it { should eq 'Hello RSpec!' }
  end
end
