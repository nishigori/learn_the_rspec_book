# -*- encoding: utf-8 -*-

class RSpecGreeter
  def greet(greeting)
    "#{greeting} RSpec!"
  end
end