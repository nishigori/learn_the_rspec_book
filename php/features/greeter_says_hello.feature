# language: ja

機能: greeterが挨拶をする

  BehatとPhpSpecの学習をする為に
  The RSpec Bookの読んでいる
  Greeterクラスが挨拶をする

  シナリオ: greeterがHelloと言う
    前提 Greeter
    もし Greetメッセージを送る
    ならば "Hello The Behat"と言う
