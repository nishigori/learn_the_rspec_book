<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

class Greeter
{
    public function greet()
    {
        return "Hello Behat!";
    }
}

/**
 * Features context.
 */
class FeatureContext extends BehatContext
{
    /**
     * @var Greeter
     */
    private $greeter;

    /**
     * @var string
     */
    private $greet;

    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

    /**
     * @Given /^Greeter$/
     */
    public function greeter()
    {
        $this->greeter = new Greeter();
    }

    /**
     * @Given /^Greetメッセージを送る$/
     */
    public function greetMu()
    {
        $this->greet = $this->greeter->greet();
    }

    /**
     * @Given /^"Hello The Behat"と言う$/
     */
    public function helloTheBehatYun()
    {
        return $this->greet == "Hello Behat";
    }

//
// Place your definition and hook methods here:
//
//    /**
//     * @Given /^I have done something with "([^"]*)"$/
//     */
//    public function iHaveDoneSomethingWith($argument)
//    {
//        doSomethingWith($argument);
//    }
//
}
